
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import React, { PureComponent } from 'react';
import { Form, Field, reduxForm } from 'redux-form';
import { Container, Row, Col, Button } from 'reactstrap';

// local dependencies
import { Logo } from '../../images';
import { RFInput } from '../../components/input';
import { AlertError } from '../../components/form-error';
import { isEmail } from '../../services/validation.service';
import { BoxLoader, Spinner } from '../../components/preloader';
import { FORGOT_PASSWORD, SIGN_UP } from '../../constants/routes';
import { selector, updateAction, setMetaAction, initializeAction, clearAction } from './reducer';

// configure
const FORM_NAME = 'signInForm';

class SignIn extends PureComponent {
    static propTypes = {
        errorMessage: PropTypes.string,
        init: PropTypes.func.isRequired,
        clear: PropTypes.func.isRequired,
        disabled: PropTypes.bool.isRequired,
        clearError: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        pageInitialized: PropTypes.bool.isRequired,
    };

    static defaultProps = {
        errorMessage: null,
    };

    componentDidMount = () => this.props.init();

    componentWillUnmount = () => this.props.clear();

    render () {
        const { handleSubmit, disabled, errorMessage, clearError } = this.props;
        return <div className="d-flex flex-row justify-content-center align-items-center h-100">
            <Container className="bg-white" style={{ width: 390, maxWidth: '95%' }}>
                <Form onSubmit={handleSubmit}>
                    <BoxLoader active={!this.props.pageInitialized}>
                        <Row className="h-100">
                            <Col xs="12" className="text-center pt-3 mb-3">
                                <Logo className="img-fluid" style={{ width: 100 }} />
                                <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
                            </Col>
                            <Col xs={{ size: 10, offset: 1 }}>
                                <Field
                                    type="text"
                                    name="username"
                                    component={RFInput}
                                    disabled={disabled}
                                    placeholder="Email Address"
                                />
                                <Field
                                    name="password"
                                    type="password"
                                    component={RFInput}
                                    disabled={disabled}
                                    placeholder="Password"
                                />
                                <Button
                                    block
                                    outline
                                    type="submit"
                                    color="primary"
                                    className="mb-3"
                                    disabled={disabled}
                                    style={{ borderRadius: 20 }}
                                >
                                    <span> LOGIN </span>
                                    <Spinner active={disabled} />
                                </Button>
                                <AlertError active message={errorMessage} onClear={clearError}/>
                            </Col>
                            <Col xs="12" className="text-center mb-3">
                                <Link to={SIGN_UP.LINK()}>
                                    <strong> Sign Up </strong>
                                </Link>
                            </Col>
                            <Col xs="12" className="text-center mb-3">
                                <Link to={FORGOT_PASSWORD.LINK()}>
                                    <strong> Forgot your password ? </strong>
                                </Link>
                            </Col>
                        </Row>
                    </BoxLoader>
                </Form>
            </Container>
        </div>;
    }
}

export default connect(
    state => ({
        disabled: selector(state).expectAnswer,
        errorMessage: selector(state).errorMessage,
        pageInitialized: selector(state).initialized,
    }),
    dispatch => ({
        clear: () => dispatch(clearAction()),
        init: () => dispatch(initializeAction()),
        onSubmit: formData => dispatch(updateAction(formData)),
        clearError: () => dispatch(setMetaAction({ errorMessage: null })),
    })
)(reduxForm({
    form: FORM_NAME,
    // enableReinitialize: true,
    validate: values => {
        const errors = {};
        if (!values.username) {
            errors.username = 'Email is required';
        } else if (!isEmail(values.username)) {
            errors.username = 'Invalid email address';
        }
        if (!values.password) {
            errors.password = 'Password is required';
        } else if (values.password.length < 8) {
            errors.password = 'Password must contain at least 8 symbol character';
        }
        return errors;
    },
})(SignIn));
