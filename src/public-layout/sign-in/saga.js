
// outsource dependencies
import { toastr } from 'react-redux-toastr';
import { fork, takeEvery, cancel, call, put } from 'redux-saga/effects';

// local dependencies
import { historyPush } from '../../store';
import { getSelfSaga } from '../../sagas';
import { TYPE, setMetaAction } from './reducer';
import { WELCOME_SCREEN } from '../../constants/routes';
import { instancePUB, instanceAPI } from '../../services/request.service';

function * initializeSaga () {
    console.log('%c SIGN_IN initialize ', 'color: #FF6766; font-weight: bolder; font-size: 12px;');
    // NOTE do nothing
    yield put(setMetaAction({ initialized: true }));
}

function * updateSaga ({ type, ...data }) {
    yield put(setMetaAction({ expectAnswer: true, errorMessage: null }));
    try {
        console.log('%c SIGN_IN update ', 'color: #FF6766; font-weight: bolder; font-size: 12px;'
            , '\n formData:', data
        );
        const session = yield call(instancePUB, { method: 'POST', url: '/auth/token', data });
        yield call(instanceAPI.setupSession, session);
        yield call(getSelfSaga);
        yield call(historyPush, WELCOME_SCREEN.LINK());
    } catch ({ message }) {
        yield call(toastr.error, 'Error', message);
        yield put(setMetaAction({ errorMessage: message }));
    }
    yield put(setMetaAction({ expectAnswer: false }));
}

/**
 * connect page sagas
 *
 * @private
 */
function * activityTasks () {
    yield takeEvery(TYPE.UPDATE, updateSaga);
    yield takeEvery(TYPE.INITIALIZE, initializeSaga);
}

/**
 * define activity behavior
 * on component unmount we fire action CLEAR to bring reducer data to default and here
 * we renew all sagas to prevent executing actions which does not finish yet
 */
export function * sagas () {
    let activity = yield fork(activityTasks);
    yield takeEvery(TYPE.CLEAR, function * () {
        yield cancel(activity);
        activity = yield fork(activityTasks);
    });
}

export default sagas;
