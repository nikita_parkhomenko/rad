
// outsource dependencies
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';
import React, { PureComponent } from 'react';

// local dependencies
import { Avatar } from '../../images';
import { WELCOME_SCREEN } from '../../constants/routes';
import { selector as appSelector } from '../../reducers';

class Layout extends PureComponent {
    static propTypes = {
        user: PropTypes.object,
        children: PropTypes.oneOfType([PropTypes.node, PropTypes.string, PropTypes.element]).isRequired
    };

    static defaultProps = { user: null };

    removeToast = () => null;

    componentDidMount = () => this.props.user && setTimeout(() => toastr.message(
        'Authorization detected',
        'Proceed as:',
        { component: props => <UserProceed
            remove={this.removeToast = props.remove}
            image={_.get(this.props.user, 'coverImage.url')}
            name={_.get(this.props.user, 'name', 'User Name')}
        /> }
    ), 100);

    componentWillUnmount = () => this.removeToast();

    render = () => this.props.children;
}

export default connect(
    state => ({
        user: appSelector(state).user
    }),
    null
)(Layout);

class UserProceed extends PureComponent {
    static propTypes = {
        remove: PropTypes.func.isRequired,
        name: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
    };

    // static defaultProps = { };

    render () {
        const { name, image, remove } = this.props;
        return <div className="d-flex align-items-center">
            <Avatar alt={name} src={image} className="flex-grow-0" style={{ width: '39px', height: '39px' }} />
            <Button color="link" onClick={remove} className="flex-grow-1" href={WELCOME_SCREEN.LINK()}>
                <strong> { name } </strong>
            </Button>
        </div>;
    }
}
