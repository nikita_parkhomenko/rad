
// outsource dependencies
import PropTypes from 'prop-types';
import Popover from 'react-popper-tooltip';
import React, { PureComponent } from 'react';
import { Alert, FormGroup, Label } from 'reactstrap';

// local dependencies
/**
 * Show form error using prepared popover
 */
export class PopoverError extends PureComponent {
    static propTypes = {
        message: PropTypes.string,
        usePopover: PropTypes.oneOf(['auto', 'top', 'right', 'bottom', 'left']),
    };

    static defaultProps = {
        message: null,
        usePopover: null,
    };

    render () {
        const { message, usePopover, children, ...attr } = this.props;
        const tooltipShown = Boolean(message) && Boolean(usePopover);
        return <Popover
            {...attr}
            placement={usePopover}
            tooltipShown={tooltipShown}
            tooltip={({ getTooltipProps, getArrowProps, tooltipRef, arrowRef, placement }) => <div
                {...getTooltipProps({ ref: tooltipRef, className: 'tooltip-container p-0' })}
            >
                <div {...getArrowProps({ ref: arrowRef, 'data-placement': placement, className: 'tooltip-arrow' })} />
                <div className="popover-body text-danger"> { this.props.message } </div>
            </div>}
        >
            { ({ getTriggerProps, triggerRef }) => <div
                { ...getTriggerProps({ ref: triggerRef })}
            > { children } </div> }
        </Popover>;
    }
}

/**
 * Show form error using prepared label
 */
export class LabelError extends PureComponent {
    static propTypes = {
        id: PropTypes.string,
        message: PropTypes.string,
    };

    static defaultProps = {
        id: null,
        message: null,
    };

    render () {
        const { message, id } = this.props;
        return !message ? null : <Label htmlFor={id} className="app-invalid-feedback"> { message } </Label>;
    }
}

/**
 * Show form error using prepared error components (bottom label or Popover)
 */
export class RFControlWrap extends PureComponent {
    static propTypes = {
        ...LabelError.propTypes,
        ...PopoverError.propTypes,
        className: PropTypes.string,
        label: PropTypes.oneOfType([PropTypes.node, PropTypes.string, PropTypes.element]),
    };

    static defaultProps = {
        ...LabelError.defaultProps,
        ...PopoverError.defaultProps,
        label: null,
        className: '',
    };

    render () {
        const { label, className, message, id, usePopover, children } = this.props;
        return <PopoverError usePopover={usePopover} message={message}>
            <FormGroup className={className}>
                { !label ? null : <label htmlFor={id}> { label } </label> }
                { children }
                { usePopover ? null : <LabelError id={id} message={message} /> }
            </FormGroup>
        </PopoverError>;
    }
}

/**
 * Show form error using prepared alert
 */
export class AlertError extends PureComponent {
    static propTypes = {
        active: PropTypes.bool,
        title: PropTypes.string,
        onClear: PropTypes.func,
        message: PropTypes.string,
        className: PropTypes.string,
    };

    static defaultProps = {
        title: 'Error: ',
        message: null,
        active: false,
        onClear: null,
        className: 'animated tada m-1',
    };

    render () {
        const { message, title, active, onClear, ...attr } = this.props;
        return <Alert color="danger" {...attr} isOpen={Boolean(message)} toggle={!active ? null : onClear}>
            <strong> { title } </strong> { message }
        </Alert>;
    }
}
