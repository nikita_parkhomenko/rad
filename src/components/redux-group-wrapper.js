
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export class ReduxGroupWrap extends PureComponent {
    static propTypes = {
        View: PropTypes.func.isRequired,
        meta: PropTypes.object.isRequired,
        fields: PropTypes.object.isRequired,
    };

    // static defaultProps = { };

    renderError = () => (!this.props.meta.error ? null : <h4 className="w-100 text-center pt-2 text-danger">
        { this.props.meta.error }
    </h4>);

    render () {
        const { View, ...attr } = this.props;
        return <>
            { this.renderError() }
            { this.props.fields.map((k, i) => <View
                {...attr}
                key={i}
                mKey={k}
                index={i}
                value={this.props.fields.get(i)}
            />)}
        </>;
    }
}

export default ReduxGroupWrap;
