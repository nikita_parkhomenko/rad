
// outsource dependencies

// local dependencies
import publicLayout from './public-layout/reducers';
import privateLayout from './private-layout/reducers';

// app data selector
export const selector = state => state.app;

/**
 * common application action types
 */
export const APP_TYPES = (prefix => ({
    PREFIX: new RegExp(prefix, 'i'),
    // simple action
    DATA: `${prefix}DATA`,
    CLEAR: `${prefix}CLEAR`,
    // complex action
    HEALTH: `${prefix}HEALTH`,
    GET_SELF: `${prefix}GET_SELF`,
    SIGN_OUT: `${prefix}SIGN_OUT`,
    INITIALIZE: `${prefix}INITIALIZE`,
    UPDATE_SELF: `${prefix}UPDATE_SELF`,
}))('@app/');
/**
 * common prepared "app" actions
 */
export const getSelfAction = options => ({ type: APP_TYPES.GET_SELF });
export const clearAppDataAction = options => ({ type: APP_TYPES.CLEAR });
export const signOutAction = options => ({ type: APP_TYPES.SIGN_OUT, ...options });
export const checkHealthAction = options => ({ type: APP_TYPES.HEALTH, ...options });
export const setAppDataAction = options => ({ type: APP_TYPES.DATA, ...options });
export const updateSelfAction = options => ({ type: APP_TYPES.UPDATE_SELF, ...options });
export const initializeAppAction = options => ({ type: APP_TYPES.INITIALIZE, ...options });

const appInitialState = {
    health: true,       // prevent redirect from page and show instead current page and it behavior - maintenance page
    ready: false,       // prevent redirect from page and show instead current page and it behavior - global preloader
    user: null,         // logged user information
};

/**
 * Connect all application reducers to "rootReducer"
 */
const rootReducer = {
    ...publicLayout,
    ...privateLayout,
    app: (state = appInitialState, action) => {
        const { type, ...options } = action;
        switch (type) {
            default: return state;
            case APP_TYPES.CLEAR: return appInitialState;
            case APP_TYPES.DATA: return { ...state, ...options };
        }
    }
};

/******************************************************
 *               DYNAMIC REDUCER
 ******************************************************/
export const TMP_TYPES = (prefix => ({
    UPDATE: `${prefix}UPDATE`,
    REMOVE: `${prefix}REMOVE`,
    CLEAR: `${prefix}CLEAR`,
    ADD: `${prefix}ADD`,
}))('@tmp/');

/**
 * common prepared "tmp" actions
 * for manipulating dynamically reducers
 */
export const clearTmp = name => ({ type: TMP_TYPES.CLEAR, name });
export const removeTmp = name => ({ type: TMP_TYPES.REMOVE, name });
export const addTmp = (name, state) => ({ type: TMP_TYPES.ADD, name, state });
export const updateTmp = (name, state) => ({ type: TMP_TYPES.UPDATE, name, state });

rootReducer.tmp = (state = {}, action) => {
    const { type, name, ...options } = action;
    if (!name) { return state; }
    const initial = `initial_${name}`;
    switch (type) {
        default: return state;
        case TMP_TYPES.CLEAR: return { ...state, [name]: state[initial] };
        case TMP_TYPES.UPDATE: return { ...state, [name]: options.state };
        case TMP_TYPES.REMOVE: return { ...state, [name]: null, [initial]: null };
        case TMP_TYPES.ADD: return { ...state, [name]: options.state, [initial]: options.state };
    }
};

export default rootReducer;
