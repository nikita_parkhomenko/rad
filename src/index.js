
// outsource dependencies
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import ReduxToastr from 'react-redux-toastr';
import { Provider, connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
// STYLES inject ...
import './style';
// polyfill
import './polyfill';
// local dependencies
import { config } from './constants';
import { history, store } from './store';
import * as ROUTES from './constants/routes';
import registerServiceWorker from './registerServiceWorker';
import { Modal as ImageCropModal } from './components/image-crop';

import NoMatch from './no-match';
import Maintenance from './maintenance';
import PublicLayout from './public-layout';
import PrivateLayout from './private-layout';
import Preloader from './components/preloader';

/**
 * Root view which should contain all common dependencies on views
 */
class RootView extends PureComponent {
    static propTypes = {
        ready: PropTypes.bool.isRequired,
        health: PropTypes.bool.isRequired,
    };

    render () {
        const { health, ready } = this.props;
        if (!health) { return <Maintenance />; }
        if (!ready) { return <Preloader active={true} />; }
        return <>
            <ConnectedRouter history={history} location={history.location}>
                <Switch>
                    <Route path={ROUTES.LAYOUT_PUBLIC} component={PublicLayout} />
                    <Route path={ROUTES.LAYOUT_PRIVATE} component={PrivateLayout} />
                    { config.DEBUG
                        // NOTE otherwise only for debug
                        ? <Route component={NoMatch} />
                        : <Redirect to={{ pathname: ROUTES.SIGN_IN.LINK(), state: { from: history.location } }}/>
                    }
                </Switch>
            </ConnectedRouter>
            <ReduxToastr
                progressBar
                timeOut={2000}
                preventDuplicates
                newestOnTop={false}
                position="top-right"
                transitionIn="fadeIn"
                transitionOut="fadeOut"
            />
            <ImageCropModal />
        </>;
    }
}

const Root = connect(state => ({ ...state.app }), null)(RootView);

ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
