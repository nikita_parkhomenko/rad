
// outsource dependencies
import qs from 'qs';
import _ from 'lodash';

// local dependencies
import { NEW_ID } from './spec';

export const LAYOUT_PUBLIC = '/public';
export const LAYOUT_PRIVATE = '/private';

/*-------------------------------------------------
        LAYOUT_PUBLIC nested routes
---------------------------------------------------*/
export const SIGN_IN = (base => ({
    ROUTE: base,
    REGEXP: new RegExp(`${base}.*`, 'i'),
    LINK: linkTo.bind({ url: () => base }),
}))(`${LAYOUT_PUBLIC}/sign-in`);

export const SIGN_UP = (base => ({
    ROUTE: base,
    REGEXP: new RegExp(`${base}.*`, 'i'),
    LINK: linkTo.bind({ url: () => base }),
}))(`${LAYOUT_PUBLIC}/sign-up`);

export const FORGOT_PASSWORD = (base => ({
    ROUTE: base,
    REGEXP: new RegExp(`${base}.*`, 'i'),
    LINK: linkTo.bind({ url: () => base }),
}))(`${LAYOUT_PUBLIC}/forgot-password`);

export const CHANGE_PASSWORD = (base => ({
    ROUTE: `${base}/:token?`,
    REGEXP: new RegExp(`${base}/.*`, 'i'),
    LINK: linkTo.bind({ url: ({ token }) => `${base}/${token||''}` }),
}))(`${LAYOUT_PUBLIC}/change-password`);

export const EMAIL_CONFIRMATION = (base => ({
    ROUTE: `${base}/:token?`,
    REGEXP: new RegExp(`${base}/.*`, 'i'),
    LINK: linkTo.bind({ url: ({ token }) => `${base}/${token||''}` }),
}))(`${LAYOUT_PUBLIC}/email-confirmation`);

/*-------------------------------------------------
        LAYOUT_PRIVATE nested routes
---------------------------------------------------*/
export const WELCOME_SCREEN = (base => ({
    ROUTE: base,
    REGEXP: new RegExp(base, 'i'),
    LINK: linkTo.bind({ url: () => base }),
}))(`${LAYOUT_PRIVATE}/welcome`);

export const USERS = (ROUTE => ({
    ROUTE,
    REGEXP: new RegExp(`${ROUTE}/.*`, 'i'),
    REGEXP_EDIT: new RegExp(`${ROUTE}/edit.*`, 'i'),
    LIST: `${ROUTE}/list`,
    EDIT: `${ROUTE}/edit/:id`,
    LINK: linkTo.bind({ url: () => (`${ROUTE}/list`) }),
    LINK_EDIT: linkTo.bind({ url: ({ id }) => `${ROUTE}/edit/${id || NEW_ID}` }),
}))(`${LAYOUT_PRIVATE}/users`);

/**
 * @example SETTINGS.LINK({ some: 'urlParam' query: {some: 'queryParam'}})
 * @param { Object } options
 * @returns { String }
 * @function linkTo
 * @private
 */
function linkTo (options) {
    options = options || {};
    // NOTE provide ability to format query params for url
    const params = _.isObject(options.query) ? qs.stringify(options.query, { addQueryPrefix: true }) : '';
    // NOTE prepare options to setting in url
    const opt = {};
    Object.keys(options).map(key => {
        if (_.isString(options[key]) || _.isNumber(options[key])) {
            opt[key] = encodeURIComponent(options[key]);
        }
        // *** array-callback-return
        return null;
    });
    // console.log('%c linkTo => ( options ) ', 'color: #0747a6; font-weight: bolder; font-size: 12px;'
    //     ,'\n options:', options
    //     ,'\n params:', params
    //     ,'\n result:', `${this.url(opt)}${params}`
    // );
    return `${this.url(opt)}${params}`;
}

