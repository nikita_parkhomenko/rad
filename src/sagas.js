
// outsource dependencies
import { fork, takeEvery, put, call, delay } from 'redux-saga/effects';

// local dependencies
import { sagas as publicLayoutSagas } from './public-layout/sagas';
import { sagas as privateLayoutSagas } from './private-layout/sagas';
import { instanceAPI, instancePUB } from './services/request.service';
import { APP_TYPES, setAppDataAction, checkHealthAction, initializeAppAction } from './reducers';

/**
 * Connect all application sagas "rootSaga"
 */
export default function * () {
    yield fork(privateLayoutSagas);
    yield fork(publicLayoutSagas);
    yield fork(appSagas);
}

/**
 * application data sagas
 */
function * appSagas () {
    yield takeEvery(APP_TYPES.GET_SELF, getSelfSaga);
    yield takeEvery(APP_TYPES.SIGN_OUT, signOutSaga);
    yield takeEvery(APP_TYPES.HEALTH, checkHealthSaga);
    yield takeEvery(APP_TYPES.INITIALIZE, appInitializeSaga);
    // NOTE initialization of app start from check health
    yield put(checkHealthAction());
}

function * appInitializeSaga () {
    try {
        const hasSession = yield call(instanceAPI.restoreSessionFromStore);
        if (hasSession) {
            yield call(getSelfSaga);
        }
    } catch ({ message }) {
        yield call(signOutSaga);
    }
    // NOTE initialization done
    yield put(setAppDataAction({ ready: true }));
}

function * checkHealthSaga () {
    try {
        const { status } = { status: 'UP', instancePUB };
        // const { status } = yield call(instancePUB, { method: 'GET', url: '/actuator/health' });
        if (status !== 'UP') {
            // NOTE API may answer "DOWN" (not ready yet)
            throw new Error('API down for maintenance');
        }
        yield put(setAppDataAction({ health: true }));
        yield put(initializeAppAction());
    } catch ({ message }) {
        yield put(setAppDataAction({ health: false }));
        // NOTE try again
        yield delay(10 * 1000);
        yield put(checkHealthAction());
    }
}

function * signOutSaga () {
    try {
        yield call(instanceAPI, { method: 'POST', url: '/auth/logout' });
    } catch ({ message }) {
        // NOTE do nothing
    }
    // NOTE clear client side session from store
    yield call(instanceAPI.setupSession, null);
    yield put(setAppDataAction({ user: null }));
}

export function * getSelfSaga () {
    try {
        const user = { name: 'I am a fake user data' };
        // const user = yield call(instanceAPI, { method: 'GET', url: 'auth/users/me' });
        yield put(setAppDataAction({ user }));
    } catch ({ message }) {
        // NOTE do nothing
        yield call(signOutSaga);
    }
}
