
// outsource dependencies
import React from 'react';

// local dependencies
import { Humanize, Truncate, Enum, UnsafeHtml, EscapeHtml, Duration } from '../src/components/filter';

export default { title: 'Filters' };

const toHumanize = 'TEXT_not_HUMANIZED atAll';
export const HumanizeText = () => <div className="p-5">
    <h3 className="mb-3"> To humanize: <small> {toHumanize} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<Humanize value="${toHumanize}" />'} </code> </h4>
    <Humanize value={toHumanize} />
    <hr />
    <h4 className="mb-3"> <code> {'<Humanize tag="h1" value="${toHumanize}" />'} </code> </h4>
    <Humanize tag="h1" value={toHumanize} />
    <hr />
</div>;

const toTruncate = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dicta nesciunt nostrum officiis quidem. Aliquam at culpa delectus exercitationem explicabo facilis laudantium maiores natus nobis officia quo, suscipit, tempore velit?';
export const TruncateText = () => <div className="p-5">
    <h3 className="mb-3"> To truncate: <small> {toTruncate} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<Truncate value="${toTruncate}" />'} </code> </h4>
    <Truncate value={toTruncate} />
    <hr />
    <h4 className="mb-3"> <code> {'<Truncate value="${toTruncate}" options={{ length: 30, end: \'-=END=-\', breakOnWord: true }} />'} </code> </h4>
    <Truncate value={toTruncate} options={{ length: 30, end: '-=END=-', breakOnWord: true }} />
    <hr />
</div>;

const toEnum = '! Make enum from text =)';
export const EnumText = () => <div className="p-5">
    <h3 className="mb-3"> To enum: <small> {toEnum} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<Enum value="${toEnum}" />'} </code> </h4>
    <Enum value={toEnum} />
    <hr />
</div>;

const durNum = 100;
export const DurationNumber = () => <div className="p-5">
    <h3 className="mb-3"> Duration: <small> {durNum} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<Duration value={durNum} />'} </code> </h4>
    <Duration value={durNum} />
    <hr />
    <h4 className="mb-3"> <code> {'<Duration value={durNum} options={{ format: \'[M]Minutes [S]seconds\' }} />'} </code> </h4>
    <Duration value={durNum} options={{ format: '[M] Minutes [S] seconds' }} />
    <hr />
    <h4 className="mb-3"> <code> {'<Duration value={durNum} options={{ format: \'_2_ Minutes _1_ seconds\', regSec: \'_1_\', regMin: \'_2_\' }} />'} </code> </h4>
    <Duration value={durNum} options={{ format: '_2_ Minutes _1_ seconds', regSec: '_1_', regMin: '_2_' }} />
    <hr />
</div>;

const htmlString = '<hr /><h1> Hello world <code>!!!</code></h1><hr />';
export const EscapeHtmlContent = () => <div className="p-5">
    <h3 className="mb-3"> Escape Html from string: <small> {htmlString} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<EscapeHtml value={htmlString} />'} </code> </h4>
    <EscapeHtml value={htmlString} />
    <hr />
</div>;

export const UnsafeHtmlContent = () => <div className="p-5">
    <h3 className="mb-3"> Html string to DOM: <small> {htmlString} </small> </h3>
    <hr />
    <h4 className="mb-3"> <code> {'<UnsafeHtml value={htmlString} />'} </code> </h4>
    <UnsafeHtml value={htmlString} />
    <hr />
</div>;
