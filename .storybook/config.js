import { configure } from '@storybook/react';
// STYLES inject ...
import '../src/style';
// polyfill
import '../src/polyfill';

configure(require.context('./', true, /[^(config.js)]/), module);
