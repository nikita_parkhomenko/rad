
// outsource dependencies
import React from 'react';
import { Button } from 'reactstrap';

// local dependencies
import { FasIcon } from '../src/components/fa-icon';
import Preloader, { BoxLoader, Spinner } from '../src/components/preloader';

export default { title: 'Preloader' };

export const Def = () => <div className="p-5">
    <h4> <code> {'<Preloader active />'} </code> </h4>
    <Preloader active />
</div>;

export const Box = () => <div className="p-5">
    <h4> <code> {'<BoxLoader active> content </BoxLoader>'} </code> </h4>
    <BoxLoader active> content </BoxLoader>
    <hr/>
    <h4> <code> {'<BoxLoader> content </BoxLoader>'} </code> </h4>
    <BoxLoader> content </BoxLoader>
    <hr/>
</div>;

export const Spinners = () => <div className="p-5">
    <h4> <code> {'<Spinner active> content </Spinner>'} </code> </h4>
    <Spinner active> content </Spinner>
    <hr />
    <h4> <code> {'<Spinner> content </Spinner>'} </code> </h4>
    <Spinner> content </Spinner>
    <hr />
    <Button color="success" disabled onClick={() => console.log('onClick')} className="ml-2">
        <Spinner active className="mr-2 text-danger">
            <FasIcon icon="plus" className="mr-2 text-danger" />
        </Spinner>
        Add some thing
    </Button>
    <Button color="success" onClick={() => console.log('onClick')} className="ml-2">
        <Spinner className="mr-2 text-danger">
            <FasIcon icon="plus" className="mr-2 text-danger" />
        </Spinner>
        Add some thing
    </Button>
    <hr />
    <Button color="danger" disabled onClick={() => console.log('onClick')} className="ml-2">
        <Spinner active className="mr-2">
            <FasIcon icon="trash-alt" className="mr-2" />
        </Spinner>
        Remove some thing
    </Button>
    <Button color="danger" onClick={() => console.log('onClick')} className="ml-2">
        <Spinner className="mr-2">
            <FasIcon icon="trash-alt" className="mr-2" />
        </Spinner>
        Remove some thing
    </Button>
</div>;
