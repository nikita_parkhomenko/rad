
// outsource dependencies
import _ from 'lodash';
import React  from 'react';
import { Field } from 'redux-form';

// local dependencies
import Radio from '../src/components/radio';
import Input from '../src/components/input';
import { WrappedConnectedReduxForm } from './helpers';
import InputColor from '../src/components/input-color';
import PercentChart from '../src/components/percent-chart-control';
import { Input as ImageCrop, Modal as ImageCropModal } from '../src/components/image-crop';

export default { title: 'Form Controls' };

export const ColorPicker = () => <WrappedConnectedReduxForm initialValues={{ test1: '#555555', test2: '#f8e71c' }}>
    <Field
        name="test1"
        usePopover="right"
        component={InputColor}
        label={<code> {'<Field name="test1" usePopover="right" component={InputColor} />'} </code>}
    />
    <Field
        name="test2"
        component={InputColor}
        label={<code> {'<Field name="test2" component={InputColor} />'} </code>}
    />
    <Field
        name="test3"
        component={InputColor}
        label={<code> {'<Field name="test3" component={InputColor} />'} </code>}
    />
</WrappedConnectedReduxForm>;

export const PercentControl = () => <WrappedConnectedReduxForm initialValues={{test1: [
    { name: 'Slice 1', percentage: 33, order: 0 },
    { name: 'Slice 2', percentage: 33, order: 1 },
    { name: 'Slice 3', percentage: 33, order: 2 },
]}}>
    <Field
        name="test1"
        usePopover="bottom"
        component={PercentChart}
        label={<code> {'<Field name="test1" usePopover="bottom" component={PercentChart} />'} </code>}
        toSchema={item => ({
            origin: item,
            name: _.get(item, 'name'),
            order: _.get(item, 'order'),
            value: _.get(item, 'percentage'),
        })}
        fromSchema={item => ({
            name: item.name,
            order: item.order,
            percentage: item.value
        })}
    />
</WrappedConnectedReduxForm>;

export const RadioControl = () => <WrappedConnectedReduxForm initialValues={{ test1: null, test2: null }}>
    <Field
        name="test1"
        component={Radio}
        label={<code> {'<Field name="test1" component={Radio} options={list} />'} </code>}
        options={[
            { label: 'One radio', value: 1 },
            { label: 'Two radio', value: 2 },
            { label: 'Three radio', value: 3 },
            { label: 'Four radio', value: 4 },
        ]}
    />
    <Field
        skipTouch
        name="test2"
        component={Radio}
        label={<code> {'<Field skipTouch name="test1" component={Radio} options={list} />'} </code>}
        options={[
            { label: 'One radio', value: 1 },
            { label: 'Two radio', value: 2 },
            { label: 'Three radio', value: 3 },
            { label: 'Four radio', value: 4 },
        ]}
    />
</WrappedConnectedReduxForm>;

export const InputControl = () => <WrappedConnectedReduxForm initialValues={{ test1: null, test2: 0 }}>
    <Field
        name="test1"
        component={Input}
        usePopover="bottom"
        label={<code> {'<Field name="test1" component={Input} options={list} usePopover="bottom" />'} </code>}
    />
    <Field
        max="10"
        min="-10"
        step="0.1"
        skipTouch
        name="test2"
        type="number"
        component={Input}
        filter={value => Number(value) || 0}
        label={<code> {'<Field type="number" skipTouch name="test1" component={Input} options={list} />'} </code>}
    />
</WrappedConnectedReduxForm>;

export const ImageCropControl = () => <WrappedConnectedReduxForm initialValues={{ test1: null }}>
    <ImageCropModal />
    <Field
        name="test1"
        label={'ImageCrop'}
        component={ImageCrop}
        cropOptions={{
            dir: 'TEST',
            minHeight: 100, // percent
            crop: {
                x: 0,
                y: 0,
                aspect: 1/3
            }
        }}
    />
</WrappedConnectedReduxForm>;
